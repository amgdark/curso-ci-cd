from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

driver = webdriver.Chrome()
driver.get('https://google.com.mx')

caja = driver.find_element(By.NAME, 'q')
caja.send_keys('webdriver python' + Keys.RETURN)

articulos = driver.find_elements(By.TAG_NAME, 'h3')

for articulo in articulos:
    if articulo.text != '':
        print(articulo.text)
