from behave import given, when, then
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


@given(u'que ingreso el nombre "{nombre}"')
def step_impl(context, nombre):

    driver = webdriver.Remote(command_executor='http://selenium-hub:4444', desired_capabilities=DesiredCapabilities.CHROME)

    driver.get('http://app:8000/mensaje/')
    caja = driver.find_element(By.NAME, 'nombre')
    caja.send_keys(nombre)
    context.driver = driver


@when(u'presiono el botón Enviar')
def step_impl(context):
    boton = context.driver.find_element(By.XPATH, '/html/body/form/button')
    boton.click()


@then(u'Puedo ver el mensaje "{esperado}"')
def step_impl(context, esperado):
    respuesta = context.driver.find_element(By.TAG_NAME, 'h2').text
    # respuesta = 'pedro'
    assert esperado in respuesta, f"{esperado} != {respuesta}"
    # context.driver.quit()
