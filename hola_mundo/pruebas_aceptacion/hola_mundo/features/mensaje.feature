Característica: Mensaje por nombre
    Como usuario del sistema hola mundo
    quiero ingresar mi nombre y me envíe un mensaje
    para saludar.

        Escenario: El nombre del mensaje es Juan
            Dado  que ingreso el nombre "Juan"
             Cuando presiono el botón Enviar
             Entonces Puedo ver el mensaje "Hola Juan"