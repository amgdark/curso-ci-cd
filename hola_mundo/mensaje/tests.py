from django.test import TestCase


class TestHola(TestCase):

    def test_smoke_test(self):
        self.assertEqual(4, 2 + 2)

    def test_url_hola_status_200(self):
        respuesta = self.client.get('/hola/')
        # print(respuesta.content)
        self.assertAlmostEqual(200, respuesta.status_code)

    def test_mensaje_hola_mundo(self):
        respuesta = self.client.get('/hola/')
        esperado = '<h1>Hola mundo</h1>'
        self.assertInHTML(esperado, str(respuesta.content))

    def test_template_correcto_hola_mundo(self):
        self.client.get('/hola/')
        esperado = 'hola.html'
        self.assertTemplateUsed(esperado)
