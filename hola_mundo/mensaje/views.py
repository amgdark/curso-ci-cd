from django.shortcuts import render


def hola(request):
    return render(request, 'hola.html')


def mensaje(request):
    sms = ''
    if request.method == 'POST':
        nombre = request.POST.get('nombre', None)
        if nombre:
            sms = f"Hola {nombre}"

    return render(request, 'mensaje.html', {'mensaje': sms})
