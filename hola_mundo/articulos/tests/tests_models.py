from django.test import TestCase
from articulos.models import Articulo


class TestArticuloModel(TestCase):

    def test_inserta_articulo(self):
        Articulo.objects.create(
            nombre='Lápiz',
            descripcion='Lápiz del No. 2',
            stock=5,
            precio=8.5
        )
        esperado = Articulo.objects.count()
        self.assertEqual(esperado, 1)
